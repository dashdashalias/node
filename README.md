# Node

Resolving dependency hell in [Node](https://nodejs.org) projects.

## Installation

See https://nodejs.dev.

## Resolving

> This requires OS specific implementation.

There are two main ways to resolve dependency hell in Node: native OS commands, or using the `dashdashalias` package.

 - [NPM / Yarn](npm.md)
 - [Linux](linux.md)
