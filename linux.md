## Linux

Linux provides us with the `ln` command:

 - [ln (man)](https://linux.die.net/man/1/ln)
 - [Question About The Symlink](https://askubuntu.com/questions/324930/question-about-the-symlink-of-the-ln-command)

```sh
# Be careful when working with relative paths
ln -s ../src node_modules/--

# Or specify a full path
ln -s $(pwd)/src node_modules/--

# Make sure the link is well established
ls -l node_modules/--
```

### Automation

You could add this to your `package.json`'s install script hooks to automate linking on deploy:

```json
{
  "scripts": {
    "install": "test ! -e node_modules/-- && ln -s ../src node_modules/--"
  }
}
```

### Clean Up

To remove the symlink, you'll need to be in the parent directory to `node_modules`:

```sh
# No trailing slash!
rm node_modules/--
```

---

Tested on [WSL Ubuntu 18.04 LTS](https://ubuntu.com/wsl).
