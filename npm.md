## NPM / Yarn

The package `dashdashalias` is meant to simplify Node deployments.

### Automation

Your `package.json` should contain an install script calling `dashdashalias`
on the source directory to alias:

```json
{
    "scripts": {
        "install": "dashdashalias src/"
    }
}
```

### Installing

The `dashdashalias` package should be installed to your `dependencies` and not
`devDependencies`:

```sh
npm install --save dashdashalias
#
# install "--/" (dash dash alias) >>> /var/www/html/src/
# success "--/" >>> /var/www/html/src/
#
```

### Manual Execution

If you don't want to add an install script to you `package.json`, you can call
the binary directly the normal way:

```sh
./node_modules/.bin/dashdashalias src/
#
# install "--/" (dash dash alias) >>> /var/www/html/src/
# warning "--/" no install script found calling dashdashalias
# success "--/" >>> /var/www/html/src/
#
```

### Clean Up

To remove the symlink, you'll need to be in the parent directory to `node_modules`:

```sh
# No trailing slash!
rm node_modules/--
```

---

Tested on [WSL Ubuntu 18.04 LTS](https://ubuntu.com/wsl)
